<?php
    trait Hewan
    {
        public $nama, $darah=50, $jumlah_Kaki, $keahlian;

        public function atraksi($elang)
        {
            return "Kemudian ".$this->nama." berlari dan meloncat untuk menangkap ".$elang->nama." yang sedang terbang tinggi untuk menghindar"."."."<br><br>";
        }
    }

    trait Fight
    {
        public $attack_Power, $defence_Power;

        public function serang($harimau)
        {
            return "Lalu  ".$this->nama." menyerang ".$harimau->nama." dan ".$harimau->nama." menyerang balik ".$this->nama."."."<br><br>";
        }

        public function diserang($harimau)
        {
            $this->darah = $this->darah-($harimau->attack_Power/$this->defence_Power);
            return " Elang terkena serangan oleh ".$harimau->nama. " yang menyebabkan darah berkurang menjadi ".$this->darah."."."<br>";
        }
    }

    class Harimau
    {
        use Hewan, Fight;

        public function GetInfoHewan()
        {
            return "Seekor ".$this->nama." yang mempunyai jumlah kaki ".$this->jumlah_Kaki. "<br><br>".
            " Memiliki keahlian ".$this->keahlian." attack power = ".$this->attack_Power.
            " dan defence power = ".$this->defence_Power."."."<br><br>";
        }
    }

    class Elang
    {
        use Hewan, Fight;

        public function GetInfoHewan()
        {
            return "Bertemu dengan seekor ".$this->nama." yang mempunyai jumlah kaki ".$this->jumlah_Kaki."<br><br>".
            " Memiliki keahlian ".$this->keahlian." attack power = ".$this->attack_Power.
            " dan defence power = ".$this->defence_Power."."."<br><br>";
        }
    }

    $harimau    = new Harimau();
    $harimau->nama          = "Harimau";
    $harimau->jumlah_Kaki    = 4;
    $harimau->keahlian      = "lari cepat";
    $harimau->attack_Power   = 7;
    $harimau->defence_Power  = 8;
    echo $harimau->GetInfoHewan();

    $elang      = new Elang();
    $elang->nama          = "Elang";
    $elang->jumlah_Kaki    = 2;
    $elang->keahlian      = "terbang ";
    $elang->attack_Power   = 10;
    $elang->defence_Power  = 5;
    echo $elang->GetInfoHewan();

   echo $elang->serang($harimau);
   echo $harimau->atraksi($elang);

  echo $elang->diserang($harimau);
?>